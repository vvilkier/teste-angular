import { Injectable } from '@angular/core';
import { BehaviorSubject, timer, Observable } from 'rxjs';
import { User } from 'src/app/shared/models/user';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private usersSubject$ : BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  public users$ = this.usersSubject$.asObservable();

  storedUsers = new BehaviorSubject<User[]>([]);
  selectedUser = new BehaviorSubject<User>(null);
  favoriteUsers: number[] = [];
  constructor(private http: HttpClient) {

  }

  getStoredUsers(): Observable<User[]> {
    if (this.storedUsers.getValue().length > 0) {
      return this.storedUsers;
    }else {
      return this.getUsers();
    }
  } 

  getUsers(): Observable<User[]> {
    this.getFavoriteUsers();
    let request = this.http.get<User[]>(`${environment.PROJECT_API}/users`);
    request.subscribe(users => {this.storedUsers.next(users)});
    return request;
  }

  getUserById(id: number): Observable<User> {
    let user = this.getStoredUsers().pipe(
      switchMap(users => {
        return users.filter(user => {return user.id == id})
      })
    )
    return user;
  }

  setStoredUsers(users: User[]) {
    this.storedUsers.next(users);
  }

  getFavoriteUsers(): number[] {
    let tempFavorites: string = localStorage.getItem('favoriteUsers');
    this.favoriteUsers = tempFavorites == null ? [] : tempFavorites.split(',').map(id => {return Number(id)});
    console.log('get item', this.favoriteUsers)
    return this.favoriteUsers
  }

  addFavoriteUser(id: number) {
    if (this.favoriteUsers.indexOf(id) === -1) {
      console.log('adixionou', id)
      this.favoriteUsers.push(id)
    }
    localStorage.setItem('favoriteUsers', this.favoriteUsers.join());
    console.log(this.favoriteUsers)
  }

  removeFavoriteUser(id: number) {
    this.favoriteUsers = localStorage.getItem('favoriteUsers').split(',').filter(idStorage => {
        if(id != Number(idStorage)) {
          return Number(idStorage)
        }
    }).map(idStorage => {return Number(idStorage)});
    console.log(this.favoriteUsers)
    localStorage.setItem('favoriteUsers', this.favoriteUsers.join());
  }

  verifyUserIsFavorite(id: number): boolean {
    return this.favoriteUsers.indexOf(id) !== -1
  }

}
