import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { User } from 'src/app/shared/models/user';
import { UsersService } from '../users.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  user$: Observable<User>;
  lat: number;
  lng: number;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usersService: UsersService) { }

  ngOnInit() {
    this.route.paramMap
      .subscribe((params: ParamMap) => {
        let id = parseInt(params.get('id'));
        this.user$ = this.usersService.getUserById(id)
        this.user$.subscribe(user => {
          this.lat = Number(user.address.geo.lat);
          this.lng = Number(user.address.geo.lng);
        })
      })
  }

  backToList() {
    this.router.navigate([``]);
  }

}
