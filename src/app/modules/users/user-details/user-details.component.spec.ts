import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailsComponent } from './user-details.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { UsersService } from '../users.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AgmCoreModule } from '@agm/core';

describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        HttpClientTestingModule,
        AgmCoreModule,
        RouterTestingModule.withRoutes(
          [{
              path: 'users/3',
              component: UserDetailsComponent
          }, ]),
      ],
      declarations: [ UserDetailsComponent ],
      providers: [
        UsersService,
        HttpClient
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
