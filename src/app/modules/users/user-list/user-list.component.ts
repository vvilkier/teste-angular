import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from '../users.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/models/user';
import { MatPaginator, MatTableDataSource, MatSort, PageEvent} from '@angular/material';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users$: Observable<User[]>;
  dataSource:MatTableDataSource<User>;
  usersListlength = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  displayedColumns: string[] = ['id', 'name', 'email', 'company', 'actions'];
  loading = true;
  searchFavorites = false;

  @ViewChild('paginator', {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit() {
    this.users$ = this.usersService.getUsers()
    this.users$.subscribe(users => {
      this.loading = false;
      this.usersListlength = users.length;
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.filterPredicate = (user: User, filter: string) => {
        return this.customFilterPredicate(user, filter)
      }
    })
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  toggleSearchFavorites(filter) {
    this.searchFavorites = !this.searchFavorites;
    if(filter == '') {
      filter = "*"
    }
    this.applyFilter(filter);
  }

  openUserDetails (user: User) {
    this.router.navigate([`/user/${user.id}`]);
  }

  isFavorite (id: string) {
    return this.usersService.verifyUserIsFavorite(Number(id));
  }

  toggleFavoriteUser (id: string) {
    if (this.isFavorite(id)) {
      this.usersService.removeFavoriteUser(Number(id));
    }else {
      this.usersService.addFavoriteUser(Number(id));
    }
  }

  customFilterPredicate (user: User, filter: string): boolean {
    let name = user.name.trim().toLowerCase();
    let id = String(user.id).trim().toLowerCase();
    let email = user.email.trim().toLowerCase();
    let company = user.company.name.trim().toLowerCase();
    if (filter == '*') {
      filter = ""
    }

    if (this.searchFavorites) {
      if (this.isFavorite(id)) {
        return !filter || name.indexOf(filter) > -1 
        || id.indexOf(filter) > -1
        || email.indexOf(filter) > -1
        || company.indexOf(filter) > -1
        
      } else {
        return false
      }
       
    }
    return !filter || name.indexOf(filter) > -1 
      || id.indexOf(filter) > -1
      || email.indexOf(filter) > -1
      || company.indexOf(filter) > -1
  };

}
