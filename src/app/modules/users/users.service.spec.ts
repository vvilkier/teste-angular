import { TestBed, getTestBed } from '@angular/core/testing';

import { UsersService } from './users.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/shared/models/user';

import { environment } from '../../../environments/environment';

const mockUsers: User[] = [
  {
    id: 1,
    name: "test user",
    username: "Bret",
    email: "Sincere@april.biz",
    address: {
      street: "Kulas Light",
      suite: "Apt. 556",
      city: "Gwenborough",
      zipcode: "92998-3874",
      geo: {
        lat: "-37.3159",
        lng: "81.1496"
      }
    },
    phone: "1-770-736-8031 x56442",
    website: "hildegard.org",
    company: {
      name: "Romaguera-Crona",
      catchPhrase: "Multi-layered client-server neural-net",
      bs: "harness real-time e-markets"
    }
  }
];

describe('UsersService', () => {
  let injector: TestBed;
  let usersService: UsersService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [ ],
      providers: [
        UsersService,
        HttpClient
      ]
    })

    injector = getTestBed();
    usersService = injector.get(UsersService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  /*it('getUserList() should return data', () => {
    usersService.getUsers().subscribe((res) => {
      expect(res).toEqual(mockUsers);
    });

    const req = httpMock.expectOne('https://jsonplaceholder.typicode.com/users');
    expect(req.request.method).toBe('GET');
    req.flush(mockUsers);
  });*/


  it('should be created', () => {
    const service: UsersService = TestBed.get(UsersService);
    expect(service).toBeTruthy();
  });

  it('should return current users list if is not empty', (done) => {
    const service: UsersService = TestBed.get(UsersService);
    
    service.storedUsers.next(mockUsers);

    service.getStoredUsers().subscribe(users => {
      expect(users).toEqual(mockUsers);
      done();
    })
  });

  it('should return list of users', (done) => {
    usersService.getUsers().subscribe(users => {
      expect(users.length).toBeGreaterThan(0);
      done();
    });
    const httpRequest = httpMock.match(req => req.method === 'GET' && req.url === `${environment.PROJECT_API}/users`)[1];
    expect(httpRequest.request.method).toBe('GET');
    httpRequest.flush(mockUsers);
  });

});
